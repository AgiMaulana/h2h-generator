package eko.tarigan;

public class Match {
    private double totalP, teamA, teamB, draw;
    private double menang, kalah, seri;

    public Match( int teamA, int teamB, int draw) {
        this.totalP = teamA + teamB + draw;
        this.teamA = teamA;
        this.teamB = teamB;
        this.draw = draw;

        menang = teamA/totalP;
        kalah = teamB/totalP;
        seri = draw/totalP;
    }

    public double getTotalP() {
        return totalP;
    }

    public double getTeamA() {
        return teamA;
    }

    public double getTeamB() {
        return teamB;
    }

    public double getDraw() {
        return draw;
    }

    public double getMenang() {
        return menang;
    }

    public double getKalah() {
        return kalah;
    }

    public double getSeri() {
        return seri;
    }
}
